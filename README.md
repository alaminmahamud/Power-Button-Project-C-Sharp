# Power-Button-Project-C-Sharp
a project where power button options are implemented. Shutdown, Restart, Sleep, Hibernate, Logoff Lock, everything. And totally Reusable

This is a Console application

## Project Structure

- PowerButtonOptions
  -- ShutDown
  -- Restart
  -- LogOff
  -- Lock
  -- Hibernate
  -- Sleep

## Features

1. Shutdown
2. Restart
3. Sleep
4. LogOff
5. Lock
6. Hibernate

and all the method are reuseable and static

and they can also be used as a library/utility class  
